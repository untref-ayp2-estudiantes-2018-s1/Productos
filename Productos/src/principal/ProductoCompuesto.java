package principal;

import java.util.ArrayList;
import java.util.ListIterator;

public class ProductoCompuesto implements Producto {
	
	private ArrayList <Producto> productos;
	
	public ProductoCompuesto (){
		
		this.productos = new ArrayList <Producto>();
	}
	
	public void agregarProducto (Producto producto){
		
		this.productos.add(producto);
	}
	
	public int calcularCosto (){
		
		ListIterator <Producto>itr = this.productos.listIterator();
		
		int sumatoria = 0;
		
		while (itr.hasNext()){
			
			sumatoria += itr.next().calcularCosto();
		}
		
		
		return sumatoria;
		
	}
	
	

}
