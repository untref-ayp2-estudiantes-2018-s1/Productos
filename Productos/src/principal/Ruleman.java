package principal;

public class Ruleman implements Producto{
	
	
	private int precio;
	
	public Ruleman (){
		
		this.precio = 0;
	}
	
	public void setPrecio (int valor){
		
		this.precio = valor;
	}
	
	public int calcularCosto (){
		
		return precio;
	}

}
