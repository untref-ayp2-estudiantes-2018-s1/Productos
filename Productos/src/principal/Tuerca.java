package principal;

public class Tuerca implements Producto{
	
	private int precio;
	
	public Tuerca (){
		
		this.precio = 0;
	}
	
	public void setPrecio (int valor){
		
		this.precio = valor;
	}
	
	public int calcularCosto (){
		
		return precio;
	}

}
