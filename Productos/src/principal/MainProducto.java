package principal;

public class MainProducto {
	
	
	public static void main (String [] args){
		
		Ruleman r1 = new Ruleman ();
		r1.setPrecio(10);
		Ruleman r2 = new Ruleman ();
		r2.setPrecio(5);
		Tuerca t1 = new Tuerca();
		t1.setPrecio(7);
		Tornillo tor1 = new Tornillo();
		tor1.setPrecio(1);
		VarillaDeAcero v1 = new VarillaDeAcero ();
		v1.setPrecio(8);
		ProductoCompuesto kit = new ProductoCompuesto ();
		kit.agregarProducto(t1);
		kit.agregarProducto(t1);
		kit.agregarProducto(t1);
		kit.agregarProducto(v1);
		kit.agregarProducto(r1);
		
		ProductoCompuesto kit2 = new ProductoCompuesto ();
		kit2.agregarProducto(r2);
		kit2.agregarProducto(kit);
		kit2.agregarProducto(tor1);
		kit2.agregarProducto(tor1);
		kit2.agregarProducto(tor1);
		
		System.out.println("Costo del kit 1: $" + Integer.toString(kit.calcularCosto()));
		System.out.println("Costo del kit 2: $" + Integer.toString(kit2.calcularCosto()));
	}

}
