package principal;

public class Tornillo implements Producto {
	
	private int precio;
	
	public Tornillo (){
		
		this.precio = 0;
	}
	
	public void setPrecio (int valor){
		
		this.precio = valor;
	}
	
	public int calcularCosto (){
		
		return precio;
	}

}
