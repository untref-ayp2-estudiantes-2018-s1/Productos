package principal;

public class VarillaDeAcero implements Producto{
	
	private int precio;
	
	public VarillaDeAcero (){
		
		this.precio = 0;
	}
	
	public void setPrecio (int valor){
		
		this.precio = valor;
	}
	
	public int calcularCosto (){
		
		return precio;
	}

}
